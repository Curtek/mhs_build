<?php
    header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$server = 'localhost';
$ftp_user_name = 'admin';
$ftp_user_pass = 'foobar';

$connection = ftp_connect($server) or die("Could not connect to $server");

$login = ftp_login($connection, $ftp_user_name, $ftp_user_pass);

ftp_chdir($connection, "bilder");

$contents = ftp_nlist($connection, "");

$picJson = json_encode($contents);

echo $picJson;

ftp_close($connection); 


?>
angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http, $ionicPopup, $state) {
  $scope.loginData = {};

  $scope.doLogin = function() {
    // Um App ohne Login zu testen, diesen Block auskommentieren und Kommentar nach diesem Block beachten
    var userCreds = JSON.stringify($scope.loginData);
    $http.post('https://192.168.178.44/php/loginHandler.php', userCreds).then(function(res){
      $scope.test = JSON.stringify(res.data);
      console.log($scope.test);
      if (res.data == 'success') {
        $state.go('app.index');
      } else {
        $scope.showLoginError();
      }
    }, function(error) {
      $scope.showLoginError();
      console.log("?");
      console.log(error);
    });

    // Um App ohne Login zu testen, diesen Block aktivieren
    // $state.go('app.index');
  }

  $scope.showLoginError = function() {
    var loginError = $ionicPopup.alert({
      title: 'Login fehlgeschlagen',
      template: 'Bitte überprüfen Sie die eingegebenen Daten'
    });
    // loginError.then(function(res){
    //
    // })
  }

})

.controller('BloodListCtrl', function($scope, $http, $ionicModal) {
  $scope.$on('$ionicView.beforeEnter', function() {

    $http.get('https://192.168.178.44/php/getQrList.php').then(function(response) {
       var test = JSON.stringify(response);
       $scope.qrList = test;
       //console.log(test);
       //console.log($scope.qrList);
       console.log('-------');
       $scope.data = response.data;
       console.log($scope.data);
       $scope.qrListData = [];
       angular.forEach($scope.data, function(data, index) {
         //console.log(data);
         data = data.toString();
         var dum = data.slice(0, -5);
         //console.log(dum);
         $scope.qrListData.push(dum);

       })
     }, function(error) {
       console.log("bla");
     });
   })

   $scope.helpBloodList = function() {
     $scope.showBloodHelpModal('templates/modal/_showBloodHelp.html');
   }

   $scope.showBloodHelpModal = function(templateUrl) {
     $ionicModal.fromTemplateUrl(templateUrl, {
       scope: $scope,
       animation: 'slide-in-up'
     }).then(function(modal) {
       $scope.modal = modal;
       $scope.modal.show();
     });
   }

   $scope.closeModal = function() {
     $scope.modal.hide();
     $scope.modal.remove()
   }

})

.controller('ScanCtrl', function($scope, $cordovaBarcodeScanner, $http, $ionicPopup, $ionicModal) {

  $scope.scanQr = function() {
      $cordovaBarcodeScanner.scan().then(function(data) {
          $scope.dummy = data.text
          $scope.qrObj = JSON.parse($scope.dummy);
          $scope.showScan();
          console.log("Barcode Format: " + data.format);
          console.log("Cancelled: " + data.cancelled);
          console.log("text:" + data.text);
          console.log("json:"+ $scope.qrObj.E);
          console.log("jsonL:" + $scope.qrObj.L)
      }, function(error) {
          console.log("An error happened: " + error);
      });
  };

  $scope.sendData = function() {
    var link = 'https://192.168.178.44/php/uploadQrData.php';
     //var link = '../php/uploadQrData.php';
    var dataJson = JSON.stringify($scope.qrObj);
    $http.post(link, dataJson).then(function(res){
      $scope.response = res.data;
      console.log($scope.response);
      $scope.qrObj = [];
      $scope.closeModal();
      $scope.showSendSuccess();
    }, function(error) {
      $scope.showSendFail();
      console.log(error);
    });
  }

  $scope.showScan = function() {
    $scope.showScanModal('templates/modal/_showScan.html');
  }

  $scope.showScanModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  }

  $scope.showSendSuccess = function() {
    var sendSuccess = $ionicPopup.alert({
      title: 'Erfolg',
      template: 'Das Senden der Daten war erfolgreich!'
    });
  }

  $scope.showSendFail = function() {
    var sendFail = $ionicPopup.alert({
      title: 'Fehler',
      template: 'Das Senden der ist fehlgeschlagen. Bitte versuchen Sie es erneut'
    });
  }
})

.controller('PictureCtrl', function($scope, $cordovaCamera, $ionicModal, $http, $ionicPopup) {
  $scope.takePicture = function() {
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 600,
            targetHeight: 800,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.imgb64 = imageData;
            $scope.imgURI = "data:image/jpeg;base64," + imageData;
            $scope.showImage();
        }, function(err) {
            // An error occured. Show a message to the user
            console.log(err);
        });
    }

    $scope.showImage = function() {
      $scope.showImageModal('templates/modal/_showImg.html');
    }

    $scope.showImageModal = function(templateUrl) {
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    }

    $scope.closeModal = function() {
      $scope.modal.hide();
      $scope.modal.remove()
    }

    $scope.sendImg = function() {
      var link = 'https://192.168.178.44/php/uploadPicture.php';
      var data = $scope.imgb64;
      $http.post(link, data).then(function(res){
        $scope.response = res.data;
        console.log($scope.response);
        $scope.closeModal();
        $scope.showSendSuccess();
      }, function(error) {
        $scope.showSendFail();
        console.log(error);
      });
    }

    $scope.showSendSuccess = function() {
      var sendSuccess = $ionicPopup.alert({
        title: 'Erfolg',
        template: 'Das Senden der Daten war erfolgreich!'
      });
    }

    $scope.showSendFail = function() {
      var sendFail = $ionicPopup.alert({
        title: 'Fehler',
        template: 'Das Senden der ist fehlgeschlagen. Bitte versuchen Sie es erneut'
      });
    }
})

.controller('PicListCtrl', function($scope, $http, $ionicModal) {
  $scope.$on('$ionicView.beforeEnter', function() {

    $http.get('https://192.168.178.44/php/getPictureList.php').then(function(response) {
       $scope.data = response.data;
       $scope.picListData = [];
       angular.forEach($scope.data, function(data, index) {
         //console.log(data);
        //  data = data.toString();
         var dum = data.slice(0, -4);
         //console.log(dum);
         $scope.picListData.push(dum);

       })
     }, function(error) {
       console.log("bla");
     });
   })

   $scope.helpPicList = function() {
     $scope.showPicHelpModal('templates/modal/_showPicHelp.html');
   }

   $scope.showPicHelpModal = function(templateUrl) {
     $ionicModal.fromTemplateUrl(templateUrl, {
       scope: $scope,
       animation: 'slide-in-up'
     }).then(function(modal) {
       $scope.modal = modal;
       $scope.modal.show();
     });
   }

   $scope.closeModal = function() {
     $scope.modal.hide();
     $scope.modal.remove()
   }

})

.controller('PicListItemCtrl', function($scope, $http, $stateParams, $ionicModal) {
  $scope.incomingPicData = $stateParams.pic_list_item;

  $scope.$on('$ionicView.beforeEnter', function() {
    var picData = $scope.incomingPicData;
    picData = picData + ".txt";

    $http.post('https://192.168.178.44/php/getPicListItem.php', picData).then(function(res){
      $scope.picBase = "data:image/jpeg;base64," + res.data;
    }, function(error) {
      console.log(error);
    });
  })

  $scope.showImageDetail = function() {
    $scope.showImageDetailModal('templates/modal/_showImgDetail.html');
  }

  $scope.showImageDetailModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  }
})

.controller('BloodListItemCtrl', function($scope, $stateParams, $http) {
  $scope.incomingData = $stateParams.blood_list_item;

  $scope.$on('$ionicView.beforeEnter', function() {
    var listData = $scope.incomingData;
    listData = listData + ".json";
    $http.post('https://192.168.178.44/php/getQrListItem.php', listData).then(function(res){
      $scope.qrDummy = res.data;

      $scope.qrDummy2 = JSON.parse($scope.qrDummy);
      $scope.qrItemData = JSON.parse($scope.qrDummy2);

      console.log($scope.qrItemData);
      console.log($scope.qrItemData.E);
    }, function(error) {
      //  $scope.showErrorAlert();
      console.log(error);
    });

  })


});

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.index', {
    url: '/index',
    views: {
      'menuContent': {
        templateUrl: 'templates/index.html'
      }
    }
  })

  .state('app.scan', {
      url: '/scan',
      views: {
        'menuContent': {
          templateUrl: 'templates/scan.html',
          controller: 'ScanCtrl'
        }
      }
    })
    .state('app.blood_list', {
      url: '/blood_list',
      views: {
        'menuContent': {
          templateUrl: 'templates/blood_list.html',
          controller: 'BloodListCtrl'
        }
      }
    })

  .state('app.blood_list_item', {
    url: '/blood_list/:blood_list_item',
    views: {
      'menuContent': {
        templateUrl: 'templates/blood_list_item.html',
        controller: 'BloodListItemCtrl'
      }
    }
  })

  .state('app.pic', {
    url: '/pic',
    views: {
      'menuContent': {
        templateUrl: 'templates/pic.html',
        controller: 'PictureCtrl'
      }
    }
  })

  .state('app.pic_list', {
    url: '/pic_list',
    views: {
      'menuContent': {
        templateUrl: 'templates/pic_list.html',
        controller: 'PicListCtrl'
      }
    }
  })

  .state('app.pic_list_item', {
    url: '/pic_list/:pic_list_item',
    views: {
      'menuContent': {
        templateUrl: 'templates/pic_list_item.html',
        controller: 'PicListItemCtrl'
      }
    }
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'AppCtrl'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
